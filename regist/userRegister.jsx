/**
 * 
 * @authors Your Name (you@example.org)
 * @date    2017-12-01 16:42:16
 * @version $Id$
 */
		var layer = layui.layer,
		$ = layui.jquery,
		element = layui.element;
		layer.photos({
			photos: '#layer-photo',
			anim: 5
		});
		//读取身份证信息
		window.ShowIdCardInfo = function (value) {
			var splitStr = value.split("|");
			$('#get-name').val(splitStr[0]);
			$('#get-sex').val(splitStr[1]);
			$('#get-idcard').val(splitStr[2]);
			$('#get-address').val(splitStr[3]);
			$('#start-time').val(splitStr[4]);
			$('#end-time').val(splitStr[5]);
			var picPath = splitStr[6];
			if (picPath.length > 0) {
				$('#idcard-img').attr('src', splitStr[6]);
				$('#idcard-img').attr('layer-src', splitStr[6]);
			} else {
				$('#idcard-img').attr('src', './libs/take.png');
				$('#idcard-img').attr('layer-src', './libs/take.png');
			}
		}
		//清空身份证信息
		window.ResetView = function () {
			$('#get-name').val("");
			$('#get-sex').val("");
			$('#get-idcard').val("");
			$('#get-address').val("");
			$('#start-time').val("");
			$('#end-time').val("");
			$('#idcard-img').attr('src', './libs/take.png');
			$('#idcard-img').attr('layer-src', './libs/take.png');
			$('#dp-success').hide();
			$('#content').val("");
		}
		//错误信息
		window.SendErrorInfo = function (value) {
			layer.msg(value);
		}
		$("#searchbtn").on('click', function () {
			var regIdk = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
			var idk = $('#content').val();
			if (!regIdk.test(idk)) {
				layer.msg('请输入合法的身份号')
			} else {
				window.external.queryIdCard(idk);
			}
		})

		window.ReadCardNum = function (str) {
			$('#dq').html(str);
			$('#dp-success').show();
		}