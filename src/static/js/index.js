var tab;

layui.config({
  base: 'static/js/',
  version: new Date().getTime()
}).use(['element', 'layer', 'navbar', 'tab', 'form'], function () {
  var element = layui.element,
    $ = layui.jquery,
    layerTips = parent.layer === undefined ? layui.layer : parent.layer,
    layer = layui.layer,
    navbar = layui.navbar(),
    form = layui.form,
    navData,
    UID,
    registStatus = 0;
  var equipmentID = GetQueryString('id');
  $.ajax({
    type: "POST",
    url: "/index.do",
    dataType: "json",
    success: function (result) {
      navData = result.tree;
      UID = result.id; //操作员ID
      $("#usename").html(result.name);
    },
    error: function (error) {
      console.log(error)
    },
    complete: function () {
      //设置navbar
      navbar.set({
        elem: '#admin-navbar-side',
        data: navData,
      });
      //渲染navbar
      navbar.render();
      //监听点击事件
      navbar.on('click(side)', function (data) {
        tab.tabAdd(data.field);
      });
    }
  });

  tab = layui.tab({
    elem: '#admin-nav-card', //设置选项卡容器
    contextMenu: true,
    maxSetting: 1,
    onSwitch: function (data) {}
  });

  tab.tabAdd({
    href: '/user/html.do',
    icon: 'fa fa-user',
    title: '掌静脉管理'
  });
  //iframe自适应
  $(window).on('resize', function () {
    var $content = $('#admin-nav-card .layui-tab-content');
    $content.height($('#admin-body').height() - 60);
    $content.find('iframe').each(function () {
      $(this).height($content.height());
    });
  }).resize();

  $('#logout').on('click', function () {
    window.location.href = "/logout.do?id=" + equipmentID;
  });

  //修改密码
  $('#edit-password').on('click', function () {
    layer.open({
      title: '修改密码',
      type: 1,
      resize: false,
      content: $('#password-layer'),
      success: function (layero, index) {
        //成功修改密码
        $('#submit-affirm').on('click', function () {
          if (!($('#old-password').val() && $('#new-password').val())) {
            layer.msg('密码不能为空');
          } else if ($('#new-password').val() !== $('#sure-password').val()) {
            layer.msg('确认密码不一致');
          } else {
            $.ajax({
              url: '/updatePwd.do',
              post: 'POST',
              data: {
                oldpwd: $('#old-password').val(),
                newpwd: $('#new-password').val()
              },
              success: function (result) {
                if (result.type) {
                  layer.close(index);
                  window.location.href = "/logout.do?id=" + equipmentID;
                }
                layer.msg(result.msg);
              },
              error: function (err) {
                console.log(err);
              },
              complete: function () {}
            });
          }
        });
      },
      end: function () {
        $('#old-password').val('');
        $('#new-password').val('');
        $('#sure-password').val('');
      }
    });
  });


  /**
   * 注册过程
   * openRegist掌静脉注册入口
   * 
   */
  var iframeUrl = '';
  window.openRegist = function () {
    layer.ready(function () {
      layer.open({
        title: '掌静脉注册',
        type: 1,
        area: ['1000px', '695px'],
        resize: false,
        content: $('#regist_wrapper'),
        success: function (layero, index) {
          iframeUrl = $('#myiframe').attr('src');
        },
        end: function () {
          resetBtn();
          $('#myiframe').attr('src', iframeUrl);
          iframeUrl = '';
        }
      });
    });
  }
  //收到掌静脉信息
  window.SocketOnMessage = function (data) {
    var msgstr = JSON.parse(data).msg;
    var msg = JSON.parse(msgstr);
    //手掌提示文字
    if (msg.handInfoTips) {
      $('.regist-tips').html(msg.handInfoTips);
    }
    //注册状态
    if (msg.registStatus) {
      if (parseInt(msg.registStatus) == 1) {
        registStatus = 1; //开始验证
      } else if (parseInt(msg.registStatus) == 2) {
        registStatus = 2; //获取到验证的手掌数据
      } else if (parseInt(msg.registStatus) == 3) {
        registStatus = 3; //验证完成掌静脉存在
        $('.regist-tips').html('采集失败，掌静脉信息已存在');
      } else if (parseInt(msg.registStatus) == 4) {
        registStatus = 4; //验证完成未注册开始注册
      } else if (parseInt(msg.registStatus) == 5) {
        registStatus = 5; //获取到掌静脉数据
        $('.regist-tips').html('掌静脉信息录入成功');
      }
    }

    //设备故障
    if (msg.registError) {
      layer.alert(msg.registError);
    }
    //信息提交状态
    if (msg.message) {
      $('.submit-status').html(msg.message);
      setTimeout(function () {
        layer.closeAll();
      }, 2000);
    }
  };
  /**
   * 注册流程
   */
  var firstStep = false;
  $('#regist_wrapper .list-title').on('click', function () {
    var index = $(this).index();
    if (index == 1 && !firstStep) {
      layer.msg('请按照注册顺序进行');
      return
    }
    if (index == 2) {
      if (!$('#name').val()) {
        layer.msg('请读取姓名');
        return
      } else if (!$('#idcard').val()) {
        layer.msg('请读取身份证');
        return
      }
    }
    if (index == 3) {
      if (registStatus == 3) {
        layer.msg('掌静脉信息已经存在');
        return
      } else if (registStatus !== 5) {
        layer.msg('未采集掌静脉信息');
        return
      }
      if (!$('#name').val()) {
         layer.msg('请读取姓名');
         return
       } else if (!$('#idcard').val()) {
         layer.msg('请读取身份证');
         return
       }
      if ($('#mobile').val()) {
        var phone = $('#mobile').val();
        if (!(/^\d{7,8}$/.test(phone)) && !(/^\d{11}$/.test(phone))) {
          layer.msg('手机号为11位或者固定号码为7~8位');
          return
        }
       }
    }

    switch (index) {
      case 0:
        sendMessage(JSON.stringify({
          "step": 1
        }));
        firstStep = true;
        break;
      case 1:
        sendMessage(JSON.stringify({
          "step": 2
        }));
        break;
      case 2:
        sendMessage(JSON.stringify({
          "step": 3
        }));
        break;
      case 3:
        postInfo();
        break;
    }
    $(this).addClass('layui-btn-disabled');
    $(this).attr("disabled", "true");
    $('#regist_wrapper .list-info').eq(index + 1).addClass('list-info-show').siblings().removeClass('list-info-show');
  });

  //身份证
  $('#takePhoto').on('click', function () {
    activexObj.CaptureClick();
  });
  //重新采集
  $('#hand-reset').on('click', function () {
    sendMessage(JSON.stringify({
      "handCancle": true
    }));
  });

  //取消整个注册过程
  $('#hand-cancle').on('click', function () {
    layer.closeAll();
  });

  //监听页面刷新
  window.onbeforeunload = function () {
    resetBtn();
  }
  //数据提交验证和传送
  function postInfo() {
    var info = {
      name: $('#name').val(),
      idCard: $('#idcard').val(),
      xb: $('#sex').val(),
      dz: $('#address').val(),
      fzjg: $('#fzjg').val(),
      mz: $('#mz').val(),
      sfzyxqS: $('#starttime').val(),
      sfzyxqE: $('#endtime').val(),
      mobile: $('#mobile').val(),
      machineid: equipmentID,
      id: UID
    }
    var allInfo = {
      submitSign: true,
      info: info
    }
    sendMessage(JSON.stringify(allInfo));
  }

  function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
      return unescape(r[2]);
    return null;
  }

  //重置按钮函数
  function resetBtn() {
    $('.list-info').removeClass('list-info-show');
    $('.list-info').eq(0).addClass('list-info-show')
    $('.list-title').removeClass("layui-btn-disabled");
    $('.list-title').removeAttr("disabled");
    $('#registErr').html("");
    $('.submit-status').html("");
    $('.regist-tips').html('');
    $('.name').val("");
    $('.idcard').val("");
    $('.sex').val("");
    $('.address').val("");
    $('.starttime').val("");
    $('.endtime').val("");
    $('.fzjg').val("")
    $('.fzjg').val("")
    $('.mobile').val("")
    firstStep = false;
    sendMessage(JSON.stringify({
      "allCancle": true
    }));
    registStatus = 0;
  }
  
  /**
   * idkRegist身份证注册入口
   */
  window.idkRegist = function () {
    layer.ready(function () {
      layer.open({
        title: '身份证注册',
        type: 1,
        area: ['1000px', '695px'],
        resize: false,
        content: $('#regist_idk'),
        success: function (layero, index) {
          iframeUrl = $('#myiframe').attr('src');
        },
        end: function () {
          resetBtn();
          $('#myiframe').attr('src', iframeUrl);
          iframeUrl = "";
        }
      });
    });
  }
  /**
   * 身份证注册流程
   */
  var firstStep = false;
  $('#regist_idk .list-title').on('click', function () {
    var index = $(this).index();
    if (index == 1 && !firstStep) {
      layer.msg('请按照注册顺序进行');
      return
    }
    if (index == 2) {
      if (!$('#namec').val()) {
        layer.msg('请读取姓名');
        return
      } else if (!$('#idcardc').val()) {
        layer.msg('请读取身份证');
        return
      }
      if ($('#mobilec').val()) {
        var phone = $('#mobilec').val();
        if (!(/^\d{7,8}$/.test(phone)) && !(/^\d{11}$/.test(phone))) {
          layer.msg('手机号为11位或者固定号码为7~8位');
          return
        }
      }
    }
    switch (index) {
      case 0:
        sendMessage(JSON.stringify({
          "step": 1
        }));
        firstStep = true;
        break;
      case 1:
        sendMessage(JSON.stringify({
          "step": 2
        }));
        break;
      case 2:
        postInfoc();
        break;
    }
   
    $(this).addClass('layui-btn-disabled');
    $(this).attr("disabled", "true");
    $('#regist_idk .list-info').eq(index + 1).addClass('list-info-show').siblings().removeClass('list-info-show');
  });

  //身份证
  $('#takePhotos').on('click', function () {
    activexObj.CaptureClick();
  });
   //数据提交验证和传送
   var statusTxt = '';
   function postInfoc() {
    var info = {
      xm: $('#namec').val(),
      sfzh: $('#idcardc').val(),
      xb: $('#sexc').val(),
      dz: $('#addressc').val(),
      fzjg: $('#fzjgc').val(),
      mz: $('#mzc').val(),
      sfzyxqS: $('#starttimec').val(),
      sfzyxqE: $('#endtimec').val(),
      mobile: $('#mobilec').val()
    }
    var myAjax = $.ajax({
      url: '/sdidcard/add.do',
      type: 'POST',
      data: info,
      timeout: 20000,
      success: function (result) {
        if (result.type) {
          //注册成功页面
          var allInfo = {
            submitSigns: true,
          }
          sendMessage(JSON.stringify(allInfo));
          statusTxt = '身份证注册成功';
        } else {
          statusTxt = result.msg;
        }
        statusTxt = result.msg;
      },
      error: function (err) {
        statusTxt = '网络不稳定...'
      },
      complete: function (XMLHttpRequest, status) {
        //注册消息
        if (status == 'timeout') {
          myAjax.abort();
          statusTxt = '请求超时...'
        }
        $('.submit-status').html(statusTxt);
        setTimeout(function () {
          layer.closeAll();
          statusTxt = '';
        }, 2000);
      }
    });
    
  }
  window.resetIdk = function () {
    $('.name').val("");
    $('.idcard').val("");
    $('.sex').val("");
    $('.address').val("");
    $('.starttime').val("");
    $('.endtime').val("");
    $('.fzjg').val("");
    $('.mz').val("");
  }

  /**
   * 掌静脉注册入口
   */

  window.zjmRegist = function () {
    layer.ready(function () {
      layer.open({
        title: '掌静脉注册',
        type: 1,
        area: ['1000px', '695px'],
        resize: false,
        content: $('#regist_zjm'),
        success: function (layero, index) {
          iframeUrl = $('#myiframe').attr('src');
        },
        end: function () {
          resetBtn();
          $('#myiframe').attr('src', iframeUrl);
          iframeUrl = "";
        }
      });
    });
  }


  /**
   * 掌静脉注册流程
   */
  $('#regist_zjm .list-title').on('click', function () {
    var index = $(this).index();
    if (index == 0) {
      if (!$('#names').val()) {
        layer.msg('请读取姓名');
        return
      } else if (!$('#idcards').val()) {
        layer.msg('请读取身份证');
        return
      }
    }
    if (index == 1) {
      if (registStatus == 3) {
        layer.msg('掌静脉已经存在');
        return
      } else if (registStatus !== 5) {
        layer.msg('请采集掌静脉信息');
        return
      }
      if (!$('#names').val()) {
        layer.msg('请读取姓名');
        return
      } else if (!$('#idcards').val()) {
        layer.msg('请读取身份证');
        return
      }
      if ($('#mobiles').val()) {
        var phone = $('#mobiles').val();
        if (!(/^\d{7,8}$/.test(phone)) && !(/^\d{11}$/.test(phone))) {
          layer.msg('手机号为11位或者固定号码为7~8位');
          return
        }
      }
    }
    switch (index) {
      case 0:
        sendMessage(JSON.stringify({
          "step": 3
        }));
        break;
      case 1:
        postInfos();
        break;
    }
    $(this).addClass('layui-btn-disabled');
    $(this).attr("disabled", "true");
    $('#regist_zjm .list-info').eq(index + 1).addClass('list-info-show').siblings().removeClass('list-info-show');
  });
  function postInfos() {
    var info = {
      name: $('#names').val(),
      idCard: $('#idcards').val(),
      xb: $('#sexs').val(),
      dz: $('#addresss').val(),
      fzjg: $('#fzjgs').val(),
      mz: $('#mzs').val(),
      sfzyxqS: $('#starttimes').val(),
      sfzyxqE: $('#endtimes').val(),
      mobile: $('#mobiles').val(),
      machineid: equipmentID,
      id: UID
    }
    var allInfo = {
      submitSign: true,
      info: info
    }
    sendMessage(JSON.stringify(allInfo));
  }
  /**
   * captureActiveX控件
   */
  var activexObj = document.getElementById("captureActiveX");
  window.activexObj = activexObj;
  // 将window设置给ActiveX
  activexObj.SetWindow(window);
  // 获取到身份证信息
  window.sendIDCardInfo = function (data) {
    var cardInfo = JSON.parse(data)
    // layer.msg('获取名字'+cardInfo.name)
    $('.name').val(cardInfo.name);
    $('.idcard').val(cardInfo.idNumber);
    $('.sex').val(cardInfo.sexStr);
    $('.address').val(cardInfo.address);
    $('.starttime').val(cardInfo.validtermOfStart);
    $('.endtime').val(cardInfo.validtermOfEnd);
    $('.fzjg').val(cardInfo.department);
    $('.mz').val(cardInfo.mz);
    sendMessage(JSON.stringify({
      "getInfoComplete": true
    }));
  }

  var sendObj = {
    "msg": ""
  };

  function sendMessage(value) {
    sendObj.msg = value;
    activexObj.sendMsg(JSON.stringify(sendObj))
  }
});
