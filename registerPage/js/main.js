/**
 * 
 * @authors Your Name (you@example.org)
 * @date    2017-09-05 11:11:22
 * @version $Id$
 */
/*first---初始页面   active---注册当前页面  complete--页面 layer-tip-show->注册成功*/

$(function () {
  /*
  	页面自适应
   */
  $(window).on('resize', function () {
    var _h = ($('body').height() - $('.header').height() - $('.content').height() - $('.nav').height()) / 5;
    $('.content').css({
      "marginTop": _h,
      "marginBottom": _h * 2
    });
  }).resize();

  /**
   * handInfo -->掌静脉信息
   * savehandRegistErr -->掌静脉设备出错信息
   * websocket对象
   * equipmentID -->设备ID
   */
  var handInfo = '';
  var savehandRegistErr = '';
  var websocket = null;
  var needRefresh = false;
  var equipmentID = GetQueryString('id');
  var againRegistStatus = false;
  var registStatus = 0;

  var sendObj = {
    "msg": ""
  };

  function sendMessage(value) {
    sendObj.msg = value;
    window.external.sendMsg(JSON.stringify(sendObj))
  }
  //收到消息
  window.SocketOnMessage = function (data) {
    var msgstr = JSON.parse(data).msg;
    var msg = JSON.parse(msgstr);
    //注册流程过程
    if (msg.step) {
      var stepIndex = parseInt(msg.step) - 1;
      $('.nav-box li').removeClass('first');
      $('.nav-box li').removeClass('active');
      $('.nav-box li').eq(stepIndex).addClass('active');
      for (var i = 0; i < stepIndex; i++) {
        $('.nav-box li').eq(i).addClass('complete');
        $('.nav-box li').eq(i).find('.status').html('已完成');
      }
      $('.content-item').removeClass('content-item-show');
      $('.content-item').eq(stepIndex + 1).addClass('content-item-show');
      if (stepIndex == 0) {
        window.external.playMusic(1);
      }
      if (stepIndex == 2) {
        //注册前验证
        activexObj.capturePalmData();
        sendMessage(JSON.stringify({
          "registStatus": 1
        }));
        registStatus = 1;
      }
    }
    //获取到了身份证号
    if (msg.getInfoComplete) {
      window.external.playMusic(2);
    }
    //信息传输提交数据到后台
    if (msg.submitSign) {
      var infoData = msg.info;
      infoData.handInfo = handInfo;
      var submitStatus = {};
      var myAjax = $.ajax({
        url: '/excluded/addUser.do',
        type: 'POST',
        data: infoData,
        timeout: 20000,
        success: function (result) {
          if (result.type) {
            //注册成功页面
            $('.content-item').eq(3).removeClass('content-item-show');
            $('.content-item').eq(4).addClass('content-item-show');
            $('.nav-box li').eq(3).addClass('active');
            $('.nav-box li').eq(2).removeClass('active');
            $('.nav-box li').eq(2).addClass('complete');
            $('.nav-box li').eq(2).find('.status').html('已完成');
            submitStatus.message = "注册成功";
            window.external.playMusic(5);
          } else {
            layer.ready(function () {
              layer.msg(result.msg, {
                icon: 2,
                time: 2000
              })
            })
            submitStatus.message = result.msg;
          }
        },
        error: function (err) {
          submitStatus.message = '网络不稳定，请稍后重新注册';
        },
        complete: function (XMLHttpRequest, status) {
          //注册消息
           if (status == 'timeout') {
             myAjax.abort()
             submitStatus.message = '请求超时，请稍后重新注册';
           }
          sendMessage(JSON.stringify(submitStatus));
        }
      });
    }
    //身份证注册
    if (msg.submitSigns) {
      $('.content-item').eq(3).removeClass('content-item-show');
      $('.content-item').eq(4).addClass('content-item-show');
      $('.nav-box li').eq(3).addClass('active');
      $('.nav-box li').eq(2).removeClass('active');
      $('.nav-box li').eq(2).addClass('complete');
      $('.nav-box li').eq(2).find('.status').html('已完成');
      $('.nav-box li').eq(1).removeClass('active');
      $('.nav-box li').eq(1).addClass('complete');
      $('.nav-box li').eq(1).find('.status').html('已完成');
      $('.nav-box li').eq(0).removeClass('active');
      $('.nav-box li').eq(0).addClass('complete');
      $('.nav-box li').eq(0).find('.status').html('已完成');
      window.external.playMusic(5);
    }
    //重新采集
    if (msg.handCancle) {
      if (registStatus == 1) {
        againRegistStatus = true;
        activexObj.cancleCapturePalm(); //取消验证
      } else if (registStatus == 4) {
        againRegistStatus = true;
        activexObj.cancleEnroll(); //取消注册
      } else {
        activexObj.capturePalmData(); //开启验证
        registStatus = 1;
        sendMessage(JSON.stringify({
          "registStatus": 1
        }));
      }
      cancleRegist();
    }

    //取消整个注册过程
    if (msg.allCancle) {
      if (registStatus == 1) {
        activexObj.cancleCapturePalm(); //取消验证
      } else if (registStatus == 4) {
        activexObj.cancleEnroll(); //取消注册
      }
      registStatus = 0;
      sendMessage(JSON.stringify({
        "registStatus": 0
      }));
      cancleLoad();
    }

    //检查刷手设备
    if (msg.registErrorState) {
      if (savehandRegistErr) {
        var handRegistErr = {
          "registError": savehandRegistErr
        }
        sendMessage(JSON.stringify(handRegistErr));
      }
    }
  };
  /*
  	cancleRegist() -->取消注册函数
  	cancleLoad() -->恢复到初始状态
  	registLoad() -->注册过程
   */
  /*取消注册函数*/
  function cancleRegist() {
    lastStr = "";
    handInfo = '';
    $('.step-three-loading').removeClass('loading-first');
    $('.step-three-loading').removeClass('loading-second');
    $('.step-three-loading').removeClass('loading-third');
    $('.step-three-loading').removeClass('loading-fourth');
    $('.loading-title-txt').html('开始采集');
    $('.regist-title').html('采集前请正确放置手掌');
  }
  /*恢复到初始状态*/
  function cancleLoad() {
    var stepTxtNum = ['第1步', '第2步', '第3步', '第4步'];
    $('.nav-box li').removeClass('active');
    $('.nav-box li').removeClass('complete');
    $('.content-item').removeClass('content-item-show');
    $('.content-item').eq(0).addClass('content-item-show');
    $('.nav-box li').addClass('first');
    $('.nav-box li').each(function (index, ele) {
      $('.nav-box li').eq(index).find('.status').html(stepTxtNum[index]);
    });
    savehandRegistErr = '';
    cancleRegist();
  }
  /*注册过程*/
  var lastStr = "";

  function registLoad(index, text) {
    var x = index;
    switch (x) {
      case 1:
        $('.step-three-loading').addClass('loading-first');
        $('.loading-title-txt').html('采集中');
        break;
      case 2:
        $('.step-three-loading').addClass('loading-second');
        $('.loading-title-txt').html('开始验证');
        break;
      case 3:
        $('.step-three-loading').addClass('loading-third');
        $('.loading-title-txt').html('验证中');
        break;
      case 4:
        $('.step-three-loading').addClass('loading-fourth');
        $('.loading-title-txt').html('采集成功');
        break;
      default:
    }
    if (text == '请放上手' || text == '请移开手') {
      if (text != lastStr) {
        switch (text) {
          case '请放上手':
            $('.ani-hand').addClass('ani-hand-active');
            window.external.playMusic(3);
            break;
          case '请移开手':
            $('.ani-hand').removeClass('ani-hand-active');
            window.external.playMusic(4);
            break;
          default:
        }
      }
    }
    lastStr = text;
    $('.regist-title').html(text);
  }

  function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
      return unescape(r[2]);
    return null;
  }
  /**
   * 注册掌静脉流程
   */
  var activexObj = document.getElementById("captureActiveX");
  activexObj.initLibraryWithMode(1);
  activexObj.SetWindow(window);
  //掌静脉设备出错信息
  window.reportError = function (value) {
    savehandRegistErr = value;
    if (savehandRegistErr) {
      var handRegistErr = {
        "registError": savehandRegistErr
      }
      sendMessage(JSON.stringify(handRegistErr));
    }
  }

  // 获取到CaptureData 回调
  window.capturePalmData = function (value) {
    registStatus = 2;
    sendMessage(JSON.stringify({
      "registStatus": 2
    }));
    $.ajax({
      url: '/excluded/vein/find.do',
      type: 'POST',
      data: {
        vein: value
      },
      success: function (data) {
        var dataResult = JSON.parse(data);
        var result = parseInt(dataResult.result_code);
        if (result == 1) {
          //数据已存在
          registStatus = 3;
          sendMessage(JSON.stringify({
            "registStatus": 3
          }));
          $('.regist-title').html('采集失败，掌静脉信息已存在');
          window.external.playMusic(6);
        } else {
          //开始注册
          activexObj.enrollBtnClick();
          registStatus = 4;
          sendMessage(JSON.stringify({
            "registStatus": 4
          }));
        }
      }
    })
  }
  //手掌数据
  window.enrollData = function (value) {
    handInfo = value;
    registStatus = 5;
    sendMessage(JSON.stringify({
      "registStatus": 5
    }));
  }
  //注册状态和提示文字（中间通用|分割）
  window.enrollStatus = function (value) {
    var StatusArr = value.split('|');
    var handNum = parseInt(StatusArr[0]);
    registLoad(handNum, StatusArr[1]);
    var handInfoTipsT = {
      "handInfoTips": StatusArr[1]
    }
    sendMessage(JSON.stringify(handInfoTipsT));
  }

  //掌静脉验证取消回调方法
  window.endCapture = function () {
    if (needRefresh) {
      needRefresh = false;
      activexObj.Ps_Sample_Apl_CS_FinishLibrary();
    }
    if (againRegistStatus) {
      againRegistStatus = false;
      activexObj.capturePalmData();
      registStatus = 1;
      sendMessage(JSON.stringify({
        "registStatus": 1
      }));
    }
  }
  //掌静脉注册取消回调方法
  window.endEnroll = function () {
    if (needRefresh) {
      needRefresh = false;
      activexObj.Ps_Sample_Apl_CS_FinishLibrary();
    }
    if (againRegistStatus) {
      againRegistStatus = false;
      activexObj.capturePalmData();
      registStatus = 1;
      sendMessage(JSON.stringify({
        "registStatus": 1
      }));
    }
  }
  //F6刷新
  window.needRefresh = function () {
    needRefresh = true;
    activexObj.cancleEnroll();
    activexObj.cancleCapturePalm();
  }
  window.finishLibrary = function () {
    //结束
  }

});
